// Aqui se establecen las rutas del url que se colocará
// y segun el url a donde se redireccionara o que función se ejecutará

const { Router } = require('express');
const router = Router();

const { getTrabajadores, postTrabajadores, getEmpresas, postEmpresas, getAsignacionTrabajadores, postAsignacionTrabajadores, getIntercambiosEmpresas, postIntercambiosEmpresas } = require('../controllers/index.controllers')

router.get('/api/trabajadores', getTrabajadores )
router.post('/api/trabajadores', postTrabajadores )

router.get('/api/empresas', getEmpresas )
router.post('/api/empresas', postEmpresas )

router.get('/api/empresas/intercambios', getIntercambiosEmpresas )
router.post('/api/empresas/intercambios', postIntercambiosEmpresas )

router.get('/api/trabajadores/asignaciones', getAsignacionTrabajadores )
router.post('/api/trabajadores/asignaciones', postAsignacionTrabajadores )

module.exports = router;