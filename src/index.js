/*
 '''
 CORPORACIÓN ELÉCTRICA NACIONAL | CORPOELEC.
 GERENCIA DE DISTRIBUCIÓN.
 Equipo de Apoyo Estratégico Nacional.
 Programación y Codificación
 Marco de trabajo para Servidores ARONIN.

 Programador           	: Yván M. Cipirán N.
 Correo Electrónico     : yvancipira@yandex.com
 Tipo Servidor de BD   	: POSTGRESQL 9.4.18
 Version del Software  	: 1.1
 Lenguaje      		 	: JAVASCRIPT
 Framework              : NODE.JS                    [Versión 12.16.3]
 Librerías Necesarias  	: EXPRESS=>                  {JSON, URLENCODED}
                          MORGAN=>                   { - }
                          NODEMON=>                  { - }
                          PG=>                       {POOL}

 Servidor url         	: 127.0.0.1
 Servidor: Puerto       : 3000
 File Encoding         	: ISO/ANSI/UTF-8
 Fecha 				 	: 24/05/2020 14:54:18 (Caracas - Venezuela)
 Encriptado			 	: Ninguno
 Seguridad Habilitada  	: Básica.
'''

*/

const express = require('express');
const app = express();

//Midleware - Software Intermedio

//Permite el uso de formatos JSON
app.use(express.json());

//Permite Interpretar el contenido enviado desde una pagina WEB (HTML, Etc)
app.use(express.urlencoded({extended:false}));

//Rutas
app.use(require('../routes/index'));

// Servidor
app.listen(3000);
console.log('Servidor ejecutandose en puerto 3000');