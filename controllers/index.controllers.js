
// Inicio la Conexión a PostgreSql
const { Pool } = require('pg');

const pool = new Pool({
    host: 'localhost',
    user: 'postgres',
    password: 'Lunes35.',
    database: 'fun_sca',
    port: '5432'
});

// FUNCIONES MAESTRAS

//Sección Trabajadores
const getTrabajadores = async (req, res) =>{
    const response = await pool.query('SELECT * FROM sca_trabajadores');
    res.status(200).json(response.rows);
}

const postTrabajadores = async (req, res) => {
    const { cedula_trabajador, np, nombre_completo, id_empresa_publica } = req.body;

    const response = await pool.query('INSERT INTO sca_trabajadores (cedula_trabajador, np, nombre_completo, id_empresa_publica) VALUES($1, $2, $3, $4)', [cedula_trabajador, np, nombre_completo, id_empresa_publica]);
    console.log(response);
    res.send('Trabajador Registrado')
};

//Inicio Sección Empresas
const getEmpresas = async (req, res) =>{
    const response = await pool.query('SELECT * FROM sca_empresas');
    res.status(200).json(response.rows);
}

const postEmpresas = async (req, res) => {
    //To do
};
// Fin de la sección empresas


// Inicio Seccion Asignaciones
const getAsignacionTrabajadores = async (req, res) =>{
    const response = await pool.query('SELECT * FROM sca_asignaciones');
    res.status(200).json(response.rows);
}

const postAsignacionTrabajadores = async (req, res) => {
    //To do

};
// Fin sección Asignaciones

// Inicio Seccion Intercambios
const getIntercambiosEmpresas = async (req, res) =>{
    const response = await pool.query('SELECT * FROM sca_asignacion_empresa');
    res.status(200).json(response.rows);
}

const postIntercambiosEmpresas = async (req, res) => {
    //To do
};

// Fin de Sección Intercambios

//Exporto los módulos para que sean utilizados desde otro ficheros
module.exports = {
    getTrabajadores,
    postTrabajadores,
    getEmpresas,
    postEmpresas,
    getAsignacionTrabajadores,
    postAsignacionTrabajadores,
    getIntercambiosEmpresas,
    postIntercambiosEmpresas
}